#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
###############################################################################
#  Syntax.py
#
#  This script was found at 
#  http://diotavelli.net/PyQtWiki/Python%20syntax%20highlighting
#  site owner (Torsten Marek)
#  and was modified by Davide <nickname17@hotmail.it>
#
###############################################################################

from PyQt4.QtCore import QRegExp
from PyQt4.QtGui import (QColor, QTextCharFormat, QFont, QSyntaxHighlighter)

def format(color, style=''):

    _color = QColor()
    _color.setNamedColor(color)

    _format = QTextCharFormat()
    _format.setForeground(_color)
    if 'bold' in style:
        _format.setFontWeight(QFont.Bold)
    if 'italic' in style:
        _format.setFontItalic(True)

    return _format

STYLES = {
    'keyword': format('#FFA500'),
    'operator': format('red'),
    'brace': format('blue'),
    'class': format('#FFA500', 'bold'),
    'communication': format('#FFA500', 'bold'),
    'string': format('#0000FF', 'bold'),
    'string2': format('#0000FF', 'bold'),
    'comment': format('darkGreen', 'italic'),
    'self': format('black', 'italic'),
    'numbers': format('brown'),
}


class ArduinoHighlighter(QSyntaxHighlighter):

    keywords = ['if', 'else', 'for', 'switch', 'case', 'while',
                'do', 'break', 'continue', 'return', 'void',
                'boolean', 'char', 'unsigned', 'int', 'byte',
                'word', 'long', 'short', 'float', 'double', 'bitRead',
                'String', 'millis', 'micros', 'delay', 'analogWrite',
                'delayMicroseconds', 'tone', 'noTone', 'shiftOut',
                'shiftIn', 'pulseIn', 'random', 'randomSeed', 'sin',
                'cos', 'tan', 'min', 'max', 'abs', 'constrain', 'map',
                'sqrt', 'static', 'size', 'lowByte','highByte', 'bit',
                'bitWrite', 'bitSet', 'bitClear', 'attachInterrupt',
                'detachInterrupt', 'interrupts', 'noInterrupts',
                'analogReference', 'analogRead', 'print', 'println',
                'digitalWrite', 'digitalRead', 'begin']

    communication = ['Serial', 'Stream', 'loop', 'setup']

    operators = [
        '=',
        '==', '!=', '<', '<=', '>', '>=',
        '\+', '-', '\*', '/', '//', '\%', '\*\*',
        '\+=', '-=', '\*=', '/=', '\%=',
        '\^', '\|', '\&', '\~', '>>', '<<',
    ]

    braces = [
        '\{', '\}', '\(', '\)', '\[', '\]',
    ]
    def __init__(self, document):
        QSyntaxHighlighter.__init__(self, document)

        self.tri_single = (QRegExp("'''"), 1, STYLES['string2'])
        self.tri_double = (QRegExp('"""'), 2, STYLES['string2'])

        rules = []

        rules += [(r'\b%s\b' % w, 0, STYLES['keyword'])
            for w in ArduinoHighlighter.keywords]
        rules += [(r'%s' % o, 0, STYLES['operator'])
            for o in ArduinoHighlighter.operators]
        rules += [(r'%s' % b, 0, STYLES['brace'])
            for b in ArduinoHighlighter.braces]
        rules += [(r'\b%s\b' % w, 0, STYLES['communication'])
            for w in ArduinoHighlighter.communication]

        rules += [
            (r'\bself\b', 0, STYLES['self']),

            (r'"[^"\\]*(\\.[^"\\]*)*"', 0, STYLES['string']),
            (r"'[^'\\]*(\\.[^'\\]*)*'", 0, STYLES['string']),

            (r'//[^\n]*', 0, STYLES['comment']),

            (r'\b[+-]?[0-9]+[lL]?\b', 0, STYLES['numbers']),
            (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, STYLES['numbers']),
            (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, STYLES['numbers']),
        ]

        self.rules = [(QRegExp(pat), index, fmt)
            for (pat, index, fmt) in rules]


    def highlightBlock(self, text):

        for expression, nth, format in self.rules:
            index = expression.indexIn(text, 0)

            while index >= 0:
                index = expression.pos(nth)
                length = expression.cap(nth).length()
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        in_multiline = self.match_multiline(text, *self.tri_single)
        if not in_multiline:
            in_multiline = self.match_multiline(text, *self.tri_double)


    def match_multiline(self, text, delimiter, in_state, style):

        if self.previousBlockState() == in_state:
            start = 0
            add = 0
        else:
            start = delimiter.indexIn(text)
            add = delimiter.matchedLength()

        while start >= 0:
            end = delimiter.indexIn(text, start + add)
            if end >= add:
                length = end - start + add + delimiter.matchedLength()
                self.setCurrentBlockState(0)
            else:
                self.setCurrentBlockState(in_state)
                length = text.length() - start + add
            self.setFormat(start, length, style)
            start = delimiter.indexIn(text, start + length)

        if self.currentBlockState() == in_state:
            return True
        else:
            return False
