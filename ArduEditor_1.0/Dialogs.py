#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
###############################################################################
#  Dialogs.py
#  
#  Copyright 2013 Davide <nickname17@hotmail.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
###############################################################################

""" Dialogs used by Editor """

# import the function expanduser from os.path library
from os.path import expanduser
# import the graphics library
from PyQt4 import QtGui


class newProject(QtGui.QDialog):
    
# Function:
#---- init (= define the UI of dialog)
#---- updatePath (= change the text in QLineEdit)
#---- changePath (= set the project path from combo box)
#---- finish (= close the dialog and return the path)
# Local Variables:
#---- self.path (= QLineEdit for display the project path - read only)
#---- self.choseBox (= QComboBox for display possible project path)
#---- self.name (= QLineEdit where the user write the name of file/project)
#---- self.finishButton (QPushButton for create a new file/project)
#---- self.mainLayout, self.firstLine, self.secondLine (= some Layout)
#---- self.folder (= default home path)
# Global Variables:
#---- listItem (= list of possible project path)
#---- x, y (= temporary variables for extract item from listItem)

    def __init__(self, listProject):
        """ define UI of Dialog"""
        QtGui.QDialog.__init__(self)
        self.path = QtGui.QLineEdit()
        self.path.setReadOnly(True)
        listItem = [("New Project", expanduser("~"))]
        for i in listProject:
            listItem.append((i.split("/")[-1], i))
        self.choseBox = QtGui.QComboBox()
        # when change the index of combobox, call changePath function
        self.choseBox.currentIndexChanged.connect(self.changePath)
        # print listItem
        for x, y in listItem:
            self.choseBox.addItem(x + "-" + y)  # populate the combobox
        self.setWindowTitle('New Project/File')  # set the windows title
        self.name = QtGui.QLineEdit()
        # when the text on self.name change, call updatePath function
        self.name.textChanged.connect(self.updatePath)
        # when Enter key pressed on self.name, call updatePath function
        self.name.returnPressed.connect(self.finish)
        self.finishButton = QtGui.QPushButton("Create!")
        # when finishButton clicked, call finish function
        self.finishButton.clicked.connect(self.finish)
        self.firstLine = QtGui.QHBoxLayout()
        self.firstLine.addWidget(QtGui.QLabel("Path: "))
        self.firstLine.addWidget(self.path)
        self.secondLine = QtGui.QHBoxLayout()
        self.secondLine.addWidget(QtGui.QLabel("Name: "))
        self.secondLine.addWidget(self.name)
        self.mainLayout = QtGui.QVBoxLayout(self)
        self.mainLayout.addWidget(self.choseBox)
        self.mainLayout.addLayout(self.firstLine)
        self.mainLayout.addLayout(self.secondLine)
        self.mainLayout.addWidget(self.finishButton)
        self.folder = expanduser("~") # set default folder (home path)

    def updatePath(self, name):
        """ change the text in QLineEdit """
        if self.folder == expanduser("~"):
            if name != '':
                self.path.setText(self.folder+"/"+name+"/"+name+".ino")
            else:
                self.folder = expanduser("~")
                self.path.setText(self.folder)
        else:
            self.path.setText(self.folder+"/"+name+".ino")

    def changePath(self, index):
        """ set the project path from combo box """
        name = self.choseBox.itemText(index).split("-")[-1]
        self.folder = name
        self.path.setText(name)

    def finish(self):
        """ close the dialog and return the path """
        self.close()
