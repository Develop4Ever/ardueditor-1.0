#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
###############################################################################
#  SerialMonitor.py
#  
#  Copyright 2013 Davide <nickname17@hotmail.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
###############################################################################

""" Serial Monitor """

# import sys library
import sys
# import time library
import time
# import serial library
import serial
# import the graphics library
from PyQt4 import QtGui, QtCore

port = "/dev/ttyUSB0" # default port
baudrate = 9600 # default baudrate

class SerialMonitor(QtGui.QDialog):
    
    """ main class, define the UI of serial Monitor"""
# Function (5):
#---- __init__ (= define the UI of window)
#---- send (= send a text message from serial)
#---- changePort (= change the current serial port)
#---- changeBaud (= change the current baudrate)
#---- new (= display in textOutput the text received from serial)
# Local Variables (11):
#---- self.port (= port)
#---- self.rangePT (= list of available port)
#---- self.baudrate (= baudrate)
#---- self.rangeBR (= list of available baudrate)
#---- self.textOutput (= a read only text area to display received data)
#---- self.sendText (= QLineEdit)
#---- self.baudComboBox (= a combo box to display a list (self.rangeBR))
#---- self.portComboBox (= a combo box to display a list (self.rangePT))
#---- self.barLayout (= an horizontal layout contains)
#---- self.mainLayout (= the man Layout (QVBoxLayout)
#---- self.DataCollector (= instance of Monitor class)
# Global Variables (1):
#---- item (= temporary variable to contains item of list)

    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.port = port
        self.rangePT = ["/dev/ttyUSB0", "/dev/ttyUSB1", "/dev/ttyUSB2"]
        self.baudrate = baudrate
        self.rangeBR = [300, 1200, 2400 , 4800 , 9600, 14400,
                        19200 , 28800 , 38400, 57600, 115200]
        self.setWindowTitle('Serial Monitor')
        self.textOutput = QtGui.QPlainTextEdit(self)
        self.textOutput.setReadOnly(True)
        self.sendText = QtGui.QLineEdit()
        self.sendText.returnPressed.connect(self.send)
        self.baudComboBox = QtGui.QComboBox()
        self.baudComboBox.currentIndexChanged.connect(self.changeBaud)
        for item in self.rangeBR:
            self.baudComboBox.addItem(str(item) + " baud")
        self.baudComboBox.setCurrentIndex(4)
        self.portComboBox = QtGui.QComboBox()
        self.portComboBox.currentIndexChanged.connect(self.changePort)
        for item in self.rangePT:
            self.portComboBox.addItem(item)
        self.portComboBox.setCurrentIndex(0)
        self.barLayout = QtGui.QHBoxLayout()
        self.barLayout.addWidget(self.sendText)
        self.barLayout.addWidget(self.baudComboBox)
        self.barLayout.addWidget(self.portComboBox)
        self.mainLayout = QtGui.QVBoxLayout(self)
        self.mainLayout.addWidget(self.textOutput)
        self.mainLayout.addLayout(self.barLayout)
        self.DataCollector = Monitor(self)
        self.connect(self.DataCollector, QtCore.SIGNAL("newText ( QString ) "), self.new)
        self.DataCollector.start()

    def send(self):
        """invia dati attraverso porta seriale (da testare)"""
        try:
            textToSend = self.sendText.text()
            Connection = serial.Serial(port, baudrate)
            Connection.write(textToSend)
        except serial.serialutil.SerialException:
            print "Impossibile collegarsi con la porta " + str(port)

    def changePort(self, index):
        """ change the port """
        global port
        port = self.rangePT[index]
        try:
            self.DataCollector.terminate()
            self.DataCollector = Monitor(self)
            self.connect(self.DataCollector, QtCore.SIGNAL("newText ( QString ) "), self.new)
            self.DataCollector.start()
        except AttributeError:
            pass

    def changeBaud(self, index):
        """ change the baudrate """
        global baudrate
        baudrate = int(self.rangeBR[index])
        try:
            self.DataCollector.terminate()
            self.DataCollector = Monitor(self)
            self.connect(self.DataCollector, QtCore.SIGNAL("newText ( QString ) "), self.new)
            self.DataCollector.start()
        except AttributeError:
            pass

    def new(self, text):
        """ display the text received from thread """
        self.textOutput.appendPlainText(text)

class Monitor(QtCore.QThread):
    """ class to run a thread and receive data from serial"""
# Function (2):
#---- __init__ (= define local variables)
#---- run (= start the thread)
# Local Variables (3):
#---- self.port (= port)
#---- self.baudrate (= baudrate)
#---- self.connection (= connect to baudrate and port)
# Global Variables (2):
#---- lChar (= list of char received from serial)
#---- char (= ----------------------------------)

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.port = port
        self.baudrate = baudrate
        try: self.connection = serial.Serial(self.port, self.baudrate)
        except serial.serialutil.SerialException:
            print "Impossibile collegarsi con la porta "+str(self.port)

    def run(self):
        """ start a cicle to receive data from serial port """
        while True:
            lChar = []
            while True:
                try:
                    char = self.connection.read()
                    if ord(char) == 10:
                        break
                    else:
                        lChar.append(char)
                except serial.serialutil.SerialException:
                    char = str(self.port)+"\t"+str(self.baudrate)
                except AttributeError:
                    char = str(self.port)+"\t"+str(self.baudrate)
            self.emit(QtCore.SIGNAL("newText(QString)"), "".join(lChar[:-1]))
            time.sleep(0.001)
