#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
###############################################################################
#  Model.py
#  
#  Copyright 2013 Davide <nickname17@hotmail.it>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
###############################################################################

""" I/O function """

# import some function from os.path
from os.path import (abspath, exists, dirname, curdir, join, split)
# import Popen, PIPE function from subprocess library
from subprocess import Popen, PIPE
# import os library
import os




# Function (4):
#---- createContent (= create a folder of project path and the file into it)
#---- readContent (= read the content of file)
#---- writeContent (= write the content on file)
#---- make (= compile or compile/upload the file)
# Local Variables (0):
#----
# Global Variables (13):
#---- filePath (= the path of file)
#---- dirPath (= the path without filename)
#---- listProject (= a list of opened project)
#---- _file (= open the file in read or write mode)
#---- fileContent (= the text contained in the file)
#---- text (= the text to write in file)
#---- action (= 0-compile, 1-compile/upload)
#---- board (= the model of Arduino board)
#---- makePath (= path of Makefile into dirPath)
#---- editorPath (= path of this Editor)
#---- command (= the command to execute to compile or upload file)
#---- make (= instance of Popen to execute 'command')
#---- result (= list contains the stdout and stderror of Popen instance)

def createContent(filePath, listProject):
    dirPath = dirname(filePath)
    if dirPath not in listProject:
        if not exists(dirPath):
            os.makedirs(dirPath)
    if '.ino' not in filePath:
        filePath = filePath + '.ino'
    writeContent(filePath, '')
    return filePath

def readContent(filePath):
    _file = open(filePath, "r")
    fileContent = _file.read()
    _file.close()
    return fileContent

def writeContent(filePath, text):
    try:
        _file = open(filePath, "w")
        _file.write(text)
        _file.close()
        return True
    except IOError:
        return False

def make(filePath, text, action, board='atmega328'):
    # Possible action: [(0, compile), (1, compile & upload)]
    writeContent(filePath, text)
    if action == 0:
        command = "make BOARD=" + board 
    if action == 1:
        command = ("make BOARD=" + board + "& make BOARD=" + board + " upload")
    dirPath = dirname(filePath)
    makePath = join(dirPath, "Makefile")
    editorPath = abspath(os.curdir)
    if not exists(makePath):
        os.link(join("files", "Makefile"), join(dirPath, "Makefile"))
    os.chdir(dirPath)
    make = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
    result = make.communicate()
    os.chdir(editorPath)
    return result
#------------------------------------------------------------------------------
