#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
###############################################################################
#  ArduEditor.py
#
#  Copyright 2013 Davide <nickname17@hotmail.it>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
###############################################################################

# import the graphics library
from PyQt4 import QtGui
# import the serial monitor
from SerialMonitor import SerialMonitor
# import some dialogs from my module
from Dialogs import newProject
# import the syntax higlight
from Syntax import ArduinoHighlighter
# import I/O function from model
from Model import createContent, readContent, writeContent, make
# import some function from os.path
from os.path import (dirname, split)
# import the sys library
import sys

class editorTab(QtGui.QWidget):

# Function:
#---- __init__ (= define the UI of tab)
# Local Variables:
#---- self (= QWidget)
#---- self.path (= the path of file you are editing)
#---- self.textArea (= QPlainTextEdit)
#---- self.logArea (= QTextEdit)
#---- self.splitView (= vertical Splitter Layout)
#---- self.layout (= vertical box Layout)
# Global Variables:
#---- font (= font to use in self.textArea)
#---- syntax (= applies syntax highlighting to self.textArea)
#---- bgColor (= background color of logArea)
#---- txtColor (= color of text in logArea)
#---- palette (= palette of color used in logArea)

    def __init__(self, path, text):
        QtGui.QWidget.__init__(self)
        self.path = path  # the path of file 
        self.textArea = QtGui.QPlainTextEdit()
        font = QtGui.QFont()
        font.setFamily('DejaVuSansMono')
        font.setPointSize(10)
        syntax = ArduinoHighlighter(self.textArea.document())
        self.textArea.setFont(font)
        self.textArea.setPlainText(text)
        self.logArea = QtGui.QTextEdit()
        bgColor = QtGui.QColor(0, 0, 0)
        txtColor = QtGui.QColor(255, 255, 255)
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Base, bgColor)
        palette.setColor(QtGui.QPalette.Text, txtColor)
        self.logArea.setPalette(palette)
        self.logArea.setReadOnly(True)
        self.logArea.setText("########## START CONSOLE ##########")
        self.splitView = QtGui.QSplitter(0x2)  # vertical splitter
        self.splitView.addWidget(self.textArea)
        self.splitView.addWidget(self.logArea)
        self.layout = QtGui.QVBoxLayout(self)  # main layout (Vertical Box)
        self.layout.addWidget(self.splitView)

class ArduEditor(QtGui.QMainWindow):

# Function:
#---- __init__ (= define UI)
#---- newFile (= ask the user to path and create a new project or file)
#---- openFile (= ask the user to path and open the file)
#---- saveFile (= save the current file in its path)mvc
#---- saveFileAs (= save the current file in new path)
#---- restoreFile (= load in the text area the saved version of current file)
#---- closeFile (= close the file and remove tab from tabWidget)
#---- undoFile (= undo the last changment)
#---- redoFile (= redo the last changment)
#---- make (= compile or upload the file)
#---- uploadFile (= call make function to upload file)
#---- compileFile (= call make function to compile file)
#---- copy (= copy the selected text in clipboard)
#---- cut (= cut the selected text)
#---- paste (= paste the text in clipboard on cursor position)
#---- selectAll (= select all text in the textArea)
#---- comment (= comment the selected text)
#---- uncomment (= uncomment the selected text)
#---- indentPP (= increase the indentation of selected text)
#---- indentMM (= decrease the indentation of selected text)
#---- addPrefix (= used by (comment, indentPP), add prefix to selected text)
#---- rmPrefix (= used by (uncomment, indentMM), rm prefix to selected text)
#---- openSMonitor (= open serial monitor in a new window)
#---- openMenu (= open context menu when dx clicked)
# Local Variables:
#---- self (= QMainWindow)
#---- self.menuFile (= menu contains the file options)
#---- self.menuEdit (= menu contains the edit options)
#---- self.menusketch (= menu contains the sketch options)
#---- self.menuBar (= bar contains menuFile, menuEdit, menuSketch)
#---- self.toolBar ( = QToolBar)
#---- self.tabWidget (= tab interface contains the tab of editor)
#---- self.listInstance (= list of active instance of editorTab)
#---- self.instance (= instance of editorTab)
# Global Variables:
#---- fileOption (= list of tuple contains label, function, shortcut)
#---- label, function, short (= temporary variable)
#---- editOption (= list of tuple contains label function and shortcut)
#---- toolOption (= list of tuple contains image, function of toolBar)

    def __init__(self):
    #---- QtGui element -------------------------------------------------------
        QtGui.QMainWindow.__init__(self)
        self.tabWidget = QtGui.QTabWidget()
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeFile)
        self.menuFile = QtGui.QMenu("File")
        self.menuEdit = QtGui.QMenu("Edit")
        self.menuSketch = QtGui.QMenu("Sketch")
        self.menuBar = QtGui.QMenuBar()
        self.toolBar = QtGui.QToolBar()
        self.setWindowTitle('ArduEditor')
        self.setMenuBar(self.menuBar)
        self.addToolBar(self.toolBar)
        self.setCentralWidget(self.tabWidget)
        self.resize(800, 500)
    #--------------------------------------------------------------------------
        fileOption = [("New\t\t Ctrl+N", self.newFile, 'Ctrl+N'),
                      ("Open\t\t Ctrl+O", self.openFile, 'Ctrl+O'),
                      ("Close\t\t Ctrl+W", self.closeFile, 'Ctrl+W'),
                      ("Save\t\t Ctrl+S", self.saveFile, 'Ctrl+S'),
                      ("Save As\t\t ", self.saveFileAs, 'Ctrl+Alt+S')]
        for label, function, short in fileOption:
            self.menuFile.addAction(label, function, short)
        editOption = [("Undo\t\t Ctrl+Z", self.undoFile, "Ctrl+Z"),
                      ("Redo\t\t Ctrl+Y", self.redoFile, "Ctrl+Y"),
                      ("Cut\t\t Ctrl+X", self.cut, "Ctrl+X"),
                      ("Copy\t\t Ctrl+C", self.copy, "Ctrl+C"),
                      ("Paste\t\t Ctrl+P", self.paste, "Ctrl+P"),
                      ("Select All\t\t Ctrl+A", self.selectAll, 
                       "Ctrl+A"),
                      ("Comment\t\t Ctrl+/", self.comment, "Ctrl+7"),
                      ("Uncomment\t\t Ctrl+Alt+/", self.uncomment, 
                       "Ctrl+Alt+7"),
                      ("Increase Indent\t\t Ctrl+[", self.indentPP, 
                       "Ctrl+["),
                      ("Decrease Indent\t\t Ctrl+]", self.indentMM, 
                       "Ctrl+]")]
        for label, function, shortcut in editOption:
            self.menuEdit.addAction(label, function, shortcut)
        sketchOption = [("Verify/Compile\t\t Ctrl+R", 
                         self.compileFile, "Ctrl+R"),
                        ("Upload\t\t Ctrl+U", self.uploadFile, 
                         "Ctrl+U")]
        for label, function, shortcut in sketchOption:
            self.menuSketch.addAction(label, function, shortcut)
        self.menuBar.addMenu(self.menuFile)
        self.menuBar.addMenu(self.menuEdit)
        self.menuBar.addMenu(self.menuSketch)
        toolOption = [("images/New.png", "New", self.newFile),
                      ("images/Open.png", "Open", self.openFile),
                      ('images/Save.png', "Save", self.saveFile),
                      ('images/SaveAs.png', "SaveAs", self.saveFileAs),
                      ('images/Restore.png', "Restore", 
                       self.restoreFile),
                      ('images/Close.png', "Close", self.closeFile),
                      ('images/Back.png', "Back", self.undoFile),
                      ('images/Next.png', "Next", self.redoFile),
                      ('images/Make.png', "Make", self.compileFile),
                      ('images/Up.png', "Upload", self.uploadFile),
                      ('images/SMonitor.png', "SerialMonitor", 
                       self.openSMonitor)]
        for image, label, function in toolOption:
            if label == "Restore":
                self.toolBar.addSeparator()
            if label == "Back":
                self.toolBar.addSeparator()
            if label == "Make":
                self.toolBar.addSeparator()
            self.toolBar.addAction(QtGui.QIcon(image), label, function)
        self.listInstance = []
        self.instance = None

    def newFile(self):
        """ Create a new sketch """
        listProject = set([dirname(str(i.path)) for i in self.listInstance])
        new = newProject(listProject)
        new.exec_()
        path = new.path.text()
        name = split(str(path))[-1]
        path = createContent(str(path), listProject)
        self.instance = editorTab(path, readContent(path))
        self.instance.textArea.setContextMenuPolicy(3)
        self.instance.textArea.customContextMenuRequested.connect(self.openMenu)
        self.listInstance.append(self.instance)
        self.tabWidget.addTab(self.listInstance[-1], name)
        self.tabWidget.setCurrentIndex(len(self.listInstance)-1)

    def openFile(self):
        """ Open an existent sketch """
        path = QtGui.QFileDialog.getOpenFileName()
        if path != '':
            self.instance = editorTab(str(path), readContent(str(path)))
            self.instance.textArea.setContextMenuPolicy(3)
            self.instance.textArea.customContextMenuRequested.connect(self.openMenu)
            self.listInstance.append(self.instance)
            self.tabWidget.addTab(self.listInstance[-1], split(str(path))[-1])
            self.tabWidget.setCurrentIndex(len(self.listInstance)-1)

    def saveFile(self):
        """ Save the current sketch """
        index = self.tabWidget.currentIndex()
        path = self.listInstance[index].path
        text = self.listInstance[index].textArea.toPlainText()
        save = writeContent(str(path), text)
        if save == True:
            print "save %s [DONE]" %(path)
        else:
            print "save %s [FAIL]" %(path)

    def saveFileAs(self):
        """ Save the current sketch in a different path """
        index = self.tabWidget.currentIndex()
        path = QtGui.QFileDialog.getSaveFileName()
        if path != '':
            text = self.listInstance[index].textArea.toPlainText()
            self.listInstance[index].path = path
            save = writeContent(str(path), text)
            self.tabWidget.setTabText(index, split(str(path))[-1])
            if save == True:
                print "save %s [DONE]" %(path)
            else:
                print "save %s [FAIL]" %(path)

    def restoreFile(self):
        """ Restore the content of the saved sketch """
        index = self.tabWidget.currentIndex()
        path = self.listInstance[index].path
        oldContent = readContent(path)
        self.listInstance[index].textArea.setPlainText(oldContent)

    def closeFile(self, index=''):
        """ Close the current sketch and remove the tab from tabWidget """
        if index == '':
            index = self.tabWidget.currentIndex()
        del self.listInstance[index]
        self.tabWidget.removeTab(index)

    def undoFile(self):
        """ Undo the last changment """
        index = self.tabWidget.currentIndex()
        self.listInstance[index].textArea.undo()

    def redoFile(self):
        """ Redo the last changment """
        index = self.tabWidget.currentIndex()
        self.listInstance[index].textArea.redo()

    def uploadFile(self):
        """ Compile and Upload the current sketch with Make function """
        self.compileUpload(1)

    def compileFile(self):
        """ Compile the current sketch with Make function """
        self.compileUpload(0)

    def compileUpload(self, action):
        """ Compile the current sketch with Make function """
        index = self.tabWidget.currentIndex()
        path = self.listInstance[index].path
        text = self.listInstance[index].textArea.toPlainText()
        _compile = make(path, text, action)
        out, error = _compile[0], _compile[1]
        self.listInstance[index].logArea.clear()
        if error != '':
            self.listInstance[index].logArea.append("<font color=#FFA500>" + 
                                                    error + "<br>" + 
                                                    "Compilazione fallita" +
                                                    "</font>")
        else:
            self.listInstance[index].logArea.append(out + "\n" + 
                                                "Compilazione terminata correttamente")
        
    def copy(self):
        """ Copy the selected text """
        index = self.tabWidget.currentIndex()
        self.listInstance[index].textEdit.copy()

    def cut(self):
        """ Cut the selected text """
        index = self.tabWidget.currentIndex()
        self.listInstance[index].textArea.cut()

    def paste(self):
        """ paste in the place of cursor the text on clipboard """
        index = self.tabWidget.currentIndex()
        self.listInstance[index].textArea.paste()

    def selectAll(self):
        """ Select all text in texArea """
        index = self.tabWidget.currentIndex()
        self.listInstance[index].textArea.selectAll()

    def comment(self):
        """ Add a comment in the selected line or block of text """
        self.addPrefix("//")

    def uncomment(self):
        """ Remove a comment in the selected line or block of text """
        self.rmPrefix("//")

    def indentPP(self):
        """ Increas the indentation in the selected line or block of text """
        self.addPrefix(" "*4)

    def indentMM(self):
        """ Decrease the indentation in the selected line or block of text """
        self.rmPrefix(" "*4)

    def addPrefix(self, prefix):
        """ Add a prefix (used by function: comment, indentPP) """
        index = self.tabWidget.currentIndex()
        cursor = self.listInstance[index].textArea.textCursor()
        if cursor.selectedText().isEmpty():
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
            cursor.insertText(prefix)
        else:
            startPosition = cursor.selectionStart()
            endPosition = cursor.selectionEnd()
            cursor.setPosition(endPosition)
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
            print startPosition
            while cursor.position() > startPosition:
                print cursor.position()
                cursor.insertText(prefix)
                cursor.movePosition(QtGui.QTextCursor.PreviousBlock)
                cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
                if cursor.position() == 0:
                    break
            cursor.insertText(prefix)

    def rmPrefix(self, prefix):
        """ Remove a prefix (used by function: uncomment, indentMM)"""
        index = self.tabWidget.currentIndex()
        cursor = self.listInstance[index].textArea.textCursor()
        if cursor.selectedText().isEmpty():
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
            cursor.setPosition(cursor.position()+len(prefix),
                               QtGui.QTextCursor.KeepAnchor)
            if unicode(cursor.selectedText()) == prefix:
                cursor.removeSelectedText()
        else:
            startPosition = cursor.selectionStart()
            endPosition = cursor.selectionEnd()
            cursor.setPosition(endPosition)
            cursor.movePosition(QtGui.QTextCursor.StartOfBlock,
                                QtGui.QTextCursor.KeepAnchor)
            oldPosition = None
            while cursor.position() >= startPosition:
                newPosition = cursor.position()
                if oldPosition == newPosition:
                    break
                else:
                    oldPosition = newPosition
                cursor.movePosition(QtGui.QTextCursor.StartOfBlock)
                cursor.setPosition(cursor.position()+len(prefix),
                                   QtGui.QTextCursor.KeepAnchor)
                if unicode(cursor.selectedText()) == prefix:
                    cursor.removeSelectedText()
                cursor.movePosition(QtGui.QTextCursor.PreviousBlock)
                cursor.movePosition(QtGui.QTextCursor.EndOfBlock)

    def openSMonitor(self):
        """ Open the Serial monitor """
        newSMonitor = SerialMonitor()
        newSMonitor.exec_()
        newSMonitor.DataCollector.terminate()

    def openMenu(self, point):
        """ Open the context menu on the dx clicked point """
        self.menuEdit.exec_(self.instance.textArea.mapToGlobal(point))

def main():
    app = QtGui.QApplication(sys.argv)
    window = ArduEditor()
    window.show()
    app.exec_()
    
if __name__ == '__main__':
    main()
